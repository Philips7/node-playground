const { app } = require('../config/app')
const { homeRouter } = require('./home/home.router')
const { usersRouter } = require('./users/users.router')
const { tokenRouter } = require('./token/token.router')
const { ordersRouter } = require('./orders/orders.router')

module.exports = {
  initRoutes: () => {
    app.use(homeRouter)
    app.use(tokenRouter)
    app.use(usersRouter)
    app.use(ordersRouter)
  },
}
