const getHome = (req, res) => {
  res.json({
    status: 'My API is dead!',
  })
}

const controller = {
  homeController: {
    getHome,
  },
}

module.exports = controller
