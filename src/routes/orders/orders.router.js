const express = require('express')
const passport = require('passport')

const ordersRouter = express.Router()

const { ordersController } = require('./orders.controller')

ordersRouter
  .route('/orders')
  .get(
    passport.authenticate('jwt', { session: false }),
    ordersController.getAll,
  )

module.exports = { ordersRouter }
