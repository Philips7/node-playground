const getAll = (req, res) => {
  res.json({ data: 'Protected', value: req.value })
}

const controller = {
  ordersController: {
    getAll,
  },
}

module.exports = controller
