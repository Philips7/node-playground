const express = require('express')

const { usersController } = require('./users.controller')
const { validateBody } = require('../../validators/validator')
const { authSchema } = require('../../validators/schemas/auth.schema')

const usersRouter = express.Router()

/**
 * @swagger
 * /users:
 *   post:
 *     description: Add new user to the application
 *     consumes:
 *       - application/json
 *     parameters:
 *       - in: body
 *         schema:
 *           type: object
 *           required:
 *             - email
 *             - password
 *           properties:
 *             email:
 *               type: string
 *             password:
 *               type: string
 *     responses:
 *       201:
 *         description: User was successfully created
 */
usersRouter
  .route('/users')
  .post(validateBody(authSchema), usersController.createOne)

module.exports = { usersRouter }
