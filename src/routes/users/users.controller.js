const HttpStatus = require('http-status-codes')
const bcrypt = require('bcryptjs')

const { User } = require('../../models/User')
const { sequelize } = require('../../config/db')

const createOne = async (req, res) => {
  const { email, password } = req.body

  try {
    bcrypt.hash(password, 10, (err, hash) => {
      if (err) {
        res.sendStatus(HttpStatus.BAD_REQUEST)
      }

      sequelize
        .sync()
        .then(() =>
          User.create({
            email: email,
            password_digest: hash,
            createdAt: new Date(),
            updateAt: new Date(),
          }),
        )
        .then(() => {
          res.sendStatus(HttpStatus.CREATED)
        })
    })
  } catch (error) {
    res.send({ errors: [error] })
  }
}

const controller = {
  usersController: {
    createOne,
  },
}

module.exports = controller
