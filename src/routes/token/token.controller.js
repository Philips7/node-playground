const HttpStatus = require('http-status-codes')
const jwt = require('jsonwebtoken')
const bcrypt = require('bcryptjs')

const { config } = require('../../config/config')
const { User } = require('../../models/User')

const getToken = async (req, res) => {
  const { email, password } = req.body

  try {
    const user = await User.findOne({
      where: {
        email,
      },
    })

    if (!user) {
      return res.status(HttpStatus.BAD_REQUEST).send('User not found')
    }

    bcrypt.compare(password, user.password_digest, (error, result) => {
      if (error) {
        res.sendStatus(HttpStatus.NOT_FOUND).send({ errors: error })
      }

      if (result) {
        const payload = { email: user.email }
        const token = jwt.sign(payload, config.secret)

        res.json({ token })
      } else {
        res.sendStatus(HttpStatus.BAD_REQUEST)
      }
    })
  } catch (error) {
    res.status(HttpStatus.UNAUTHORIZED).send({ errors: error })
  }
}

const controller = {
  tokenController: {
    getToken,
  },
}

module.exports = controller
