const express = require('express')

const { tokenController } = require('./token.controller')
const { validateBody } = require('../../validators/validator')
const { authSchema } = require('../../validators/schemas/auth.schema')

const tokenRouter = express.Router()

tokenRouter
  .route('/token')
  .post(validateBody(authSchema), tokenController.getToken)

module.exports = { tokenRouter }
