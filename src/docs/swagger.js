const swaggerUi = require('swagger-ui-express')
const swaggerJSDoc = require('swagger-jsdoc')

const { app } = require('../config/app')

const swaggerDefinition = {
  info: {
    title: 'Node Swagger API',
    version: '1.0.0',
    description: 'Demonstrating how to describe a RESTful API with Swagger',
  },
  host: 'localhost:3000',
  basePath: '/',
}

const options = {
  swaggerDefinition: swaggerDefinition,
  apis: ['./**/routes/**/*.js'],
}

const swaggerSpec = swaggerJSDoc(options)

module.exports = {
  generateDocs: () => {
    app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec))

    app.get('/swagger.json', (req, res) => {
      res.setHeader('Content-Type', 'application/json')
      res.send(swaggerSpec)
    })
  },
}
