const config = {
  secret: process.env.SECRET,
  port: process.env.PORT || 3000,
}

module.exports = { config }
