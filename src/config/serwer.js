require('dotenv').config()

const bodyParser = require('body-parser')

const { app } = require('./app')
const { auth } = require('../auth/auth')
const { config } = require('./config')
const { initRoutes } = require('../routes/routes')
const { generateDocs } = require('../docs/swagger')

app.use(bodyParser.json())
app.use(auth.initialize())

initRoutes()
generateDocs()

app.listen(config.port, () => console.log(`Listening on port ${config.port}`))
