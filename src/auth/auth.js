const passport = require('passport')
const passportJWT = require('passport-jwt')

const { config } = require('../config/config')
const { User } = require('../models/User')

const JwtStrategy = passportJWT.Strategy
const ExtractJwt = passportJWT.ExtractJwt

const opts = {
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: config.secret,
}

const strategy = new JwtStrategy(opts, async (payload, next) => {
  try {
    const user = await User.findOne({
      where: {
        email: payload.email,
      },
    })

    if (!user) {
      return next(null, false)
    }

    return next(null, User)
  } catch (error) {
    return next(error, false)
  }
})

passport.use(strategy)

const auth = {
  initialize: () => passport.initialize(),
}

module.exports = { auth }
