const HttpStatus = require('http-status-codes')
const joi = require('joi')

const validateBody = schema => {
  return (req, res, next) => {
    const result = joi.validate(req.body, schema)

    if (result.error) {
      return res.status(HttpStatus.BAD_REQUEST).json(result.error)
    }

    if (!req.value) {
      req.value = {}
    }

    req.value.body = result.value
    next()
  }
}

module.exports = { validateBody }
