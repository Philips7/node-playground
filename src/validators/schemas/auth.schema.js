const joi = require('joi')

const authSchema = joi.object().keys({
  email: joi
    .string()
    .email()
    .required(),
  password: joi.string().required(),
})

module.exports = { authSchema }
