const Sequelize = require('sequelize')

const { sequelize } = require('../config/db')

const User = sequelize.define('users', {
  email: Sequelize.STRING,
  password_digest: Sequelize.STRING,
  createdAt: Sequelize.DATE,
  updatedAt: Sequelize.DATE,
})

module.exports = { User }
