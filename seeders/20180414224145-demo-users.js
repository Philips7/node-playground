'use strict';

module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.bulkInsert('users', [{
            email: 'test@test.com',
            password_digest: '$2a$10$EgTk2vzNYK1JZLKy//UTaO6QxHGnIftfaxyupIuUVlvZQo831LOra',
            createdAt: new Date(),
            updatedAt: new Date(),
        }], {});
    },

    down: (queryInterface, Sequelize) => {
        return queryInterface.bulkDelete('users', null, {});
    }
};