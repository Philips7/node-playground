module.exports = {
  printWidth: 80,
  parser: 'flow',
  trailingComma: 'all',
  semi: false,
  singleQuote: true,
  tabWidth: 2,
  arrowParens: 'avoid',
}
