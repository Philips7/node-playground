module.exports = {
    development: {
        client: process.env.CLIENT,
        connection: {
            host: process.env.DB_HOST,
            user: process.env.DB_USER,
            password: process.env.DB_PASSWORD,
            database: process.env.DB_NAME,
            socketPath: process.env.SOCKET_PATH,
        },
        pool: {
            min: 2,
            max: 10,
        },
        migrations: {
            tableName: process.env.MYSQL_DATABASE,
        },
    },
}